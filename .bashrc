export PATH="$HOME/.cargo/bin:$HOME/.local/bin:$PATH"
export EDITOR=vim
export VISUAL="$EDITOR"
export PAGER='less -FRX'
export DOTNET_CLI_TELEMETRY_OPTOUT=1


# If not running interactively, stop now
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ll='ls -la'
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

source /usr/share/doc/pkgfile/command-not-found.bash
shopt -s checkwinsize

history_dir="${HOME}/.history/$(date +%Y/%m)"
mkdir -p "$history_dir"
HISTFILE="$history_dir/$(date --iso-8601=seconds)_${HOSTNAME}_$$"
export HISTSIZE="NOTHING"
export HISTFILESIZE="NOTHING"
export HISTTIMEFORMAT='%F %T '
shopt -s histappend
shopt -s histverify
shopt -s cmdhist
export PROMPT_COMMAND="history -a;"
all_hist_tmp=$(mktemp)
find "${HOME}"/.history/ -type f -printf "%T@ %p\0" | sort -zn | cut -zd' ' -f 2- | xargs -0 cat > "$all_hist_tmp"
history -n "$all_hist_tmp"
rm "$all_hist_tmp"
alias nohistory="HISTFILE=/dev/null"

export DOTNET_CLI_TELEMETRY_OPTOUT=1

export PATH=$PATH:~/.cabal/bin
export PATH="$PATH:$HOME/.node/bin"

if command -v direnv &> /dev/null
then
    eval "$(direnv hook bash)"
fi

source "$HOME/.ps3000/ps3000.sh"

